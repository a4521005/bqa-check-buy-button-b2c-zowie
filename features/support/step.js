const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
// const request = require("request-promise-native");

const cheerio = require("cheerio");
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1200,height:1000})
    await this.page.setDefaultTimeout(600000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(600000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})
//G6
//Benq.com
const g6BenqcomUrl=[]
const g6BenqcomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "buy.html"
            if(url.indexOf(publishUrl)!==-1 && url.indexOf("campaign")<1 || url.indexOf('/monitor/accessory/gc01.html')>0){
                g6BenqcomUrl.push(url)
            }
            //https://www.benq.com/en-us/campaign/instashow-try-and-buy.html
    })
    return g6BenqcomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on G6 BenQ.com",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g6BenqcomUrlReturnedData = await g6BenqcomUrlCollect();
    console.log(g6BenqcomUrlReturnedData)
    for(let i =0; i<g6BenqcomUrlReturnedData.length; i++){
        const g6BenqcomUrlCheck = g6BenqcomUrlReturnedData[i]
        await this.page.goto("https://www.benq.com/en-us/index.html")
        await this.page.goto(g6BenqcomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        
        //if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0) //
        if(innerHtml.indexOf("g6-btn-sm-solid-black")>0 || innerHtml.indexOf("purchase-platform-container")>0 || innerHtml.indexOf('<button class="discontinued">Discontinued</button>')>0 ){
            if(innerHtml.indexOf("g6-btn-sm-solid-black")>0){
                // await this.page.waitForSelector('div.g6-product-info-component > section.buy-section > div.container > button')
                const buyButton = await this.page.$eval('button.g6-btn-sm-solid-black', element => element.innerHTML);
                // console.log(`Pass URL: ${g6BenqcomUrlCheck}`)
                console.log(g6BenqcomUrlCheck, buyButton)
                pass.push(g6BenqcomUrlCheck)
            }else if(innerHtml.indexOf('<button class="discontinued">Discontinued</button>')>0){
                const buyButton = await this.page.$eval('section.buy-section > div.container > button.discontinued', element => element.innerHTML);
                // console.log(`Pass URL: ${g6BenqcomUrlCheck}`)
                console.log(g6BenqcomUrlCheck, buyButton)
                pass.push(g6BenqcomUrlCheck)
            }else if(innerHtml.indexOf("purchase-platform-container visible")>0 && innerHtml.indexOf("Notify Me")>0){
                const buyButton = await this.page.$eval('div.purchase-platform-container.visible > div > a', element => element.innerHTML);
                // console.log(`Pass URL: ${g6BenqcomUrlCheck}`)
                console.log(g6BenqcomUrlCheck, buyButton)
                pass.push(g6BenqcomUrlCheck)
            }else{
                // console.log(`Pass URL: ${g6BenqcomUrlCheck}`)
                pass.push(g6BenqcomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g6BenqcomUrlCheck}`)
            fail.push(g6BenqcomUrlCheck)
        }
    }
    console.log("G6 Benq.com pass url",pass)
    console.log("G6 Benq.com fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //G6
// //Benq.com
// const g6BenqcomUrl=[]
// const g6BenqcomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "buy.html"
//             if(url.indexOf(publishUrl)!==-1 && url.indexOf("campaign")<1){
//                 g6BenqcomUrl.push(url)
//             }
//             //https://www.benq.com/en-us/campaign/instashow-try-and-buy.html
//     })
//     return g6BenqcomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on G6 BenQ.com",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g6BenqcomUrlReturnedData = await g6BenqcomUrlCollect();
//     console.log(g6BenqcomUrlReturnedData)
//     for(let i =0; i<g6BenqcomUrlReturnedData.length; i++){
//         const g6BenqcomUrlCheck = g6BenqcomUrlReturnedData[i]
//         await this.page.goto("https://www.benq.com/en-us/index.html")
//         await this.page.goto(g6BenqcomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);
//         if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g6BenqcomUrlCheck}`)
//             pass.push(g6BenqcomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g6BenqcomUrlCheck}`)
//             fail.push(g6BenqcomUrlCheck)
//         }
//     }
//     console.log("G6 Benq.com pass url",pass)
//     console.log("G6 Benq.com fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })

//從這開始改成撈取Buy Button資料
//G5
//speaker
//Benq.com
const g5SpeakerBenqcomUrl=[]
const g5SpeakerBenqcomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "https://www.benq.com/en-us/speaker/"
            if(url.indexOf(publishUrl)!==-1){
                if(url.indexOf("desktop-dialogue-speaker/")!==-1 || url.indexOf("electrostatic-bluetooth-speaker/")!==-1 || url.indexOf("accessory/")!==-1 ){
                    if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
                        g5SpeakerBenqcomUrl.push(url)
                    }
                }
            }
    })
    return g5SpeakerBenqcomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on G5 BenQ.com - Speaker",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g5SpeakerBenqcomUrlReturnedData = await g5SpeakerBenqcomUrlCollect();
    console.log(g5SpeakerBenqcomUrlReturnedData)
    for(let i =0; i<g5SpeakerBenqcomUrlReturnedData.length; i++){
        const g5SpeakerBenqcomUrlCheck = g5SpeakerBenqcomUrlReturnedData[i]
        await this.page.goto("https://www.benq.com/en-us/index.html")
        await this.page.goto(g5SpeakerBenqcomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        //if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0)
        if(innerHtml.indexOf("buy-now-btn")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
            if(innerHtml.indexOf("buy-now-btn")>0){
                const buyButton = await this.page.$eval('#simpleFlow > span.buy-now-btn', element => element.innerHTML);
                console.log(g5SpeakerBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5SpeakerBenqcomUrlCheck}`)
                pass.push(g5SpeakerBenqcomUrlCheck)
            }else if(innerHtml.indexOf("Notify Me")>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5SpeakerBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5SpeakerBenqcomUrlCheck}`)
                pass.push(g5SpeakerBenqcomUrlCheck)
            }else{
                console.log(`Pass URL: ${g5SpeakerBenqcomUrlCheck}`)
                pass.push(g5SpeakerBenqcomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g5SpeakerBenqcomUrlCheck}`)
            fail.push(g5SpeakerBenqcomUrlCheck)
        }
    }
    //console.log("Benq.com pass url",pass)
    console.log("Benq.com fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //G5
// //speaker
// //Benq.com
// const g5SpeakerBenqcomUrl=[]
// const g5SpeakerBenqcomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "https://www.benq.com/en-us/speaker/"
//             if(url.indexOf(publishUrl)!==-1){
//                 if(url.indexOf("desktop-dialogue-speaker/")!==-1 || url.indexOf("electrostatic-bluetooth-speaker/")!==-1 || url.indexOf("accessory/")!==-1 ){
//                     if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
//                         g5SpeakerBenqcomUrl.push(url)
//                     }
//                 }
//             }
//     })
//     return g5SpeakerBenqcomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on G5 BenQ.com - Speaker",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g5SpeakerBenqcomUrlReturnedData = await g5SpeakerBenqcomUrlCollect();
//     console.log(g5SpeakerBenqcomUrlReturnedData)
//     for(let i =0; i<g5SpeakerBenqcomUrlReturnedData.length; i++){
//         const g5SpeakerBenqcomUrlCheck = g5SpeakerBenqcomUrlReturnedData[i]
//         await this.page.goto("https://www.benq.com/en-us/index.html")
//         await this.page.goto(g5SpeakerBenqcomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);

//         if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g5SpeakerBenqcomUrlCheck}`)
//             pass.push(g5SpeakerBenqcomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g5SpeakerBenqcomUrlCheck}`)
//             fail.push(g5SpeakerBenqcomUrlCheck)
//         }
//     }
//     console.log("Benq.com pass url",pass)
//     console.log("Benq.com fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })


//G5
//accessory
//Benq.com
const g5accessoryBenqcomUrl=[]
const g5accessoryBenqcomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "/accessory/"
            if(url.indexOf(publishUrl)!==-1){
                if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0 && url.indexOf("all.html")<0 && url.indexOf('https://www.benq.com/en-us/monitor/accessory/gc01.html')<0){
                    g5accessoryBenqcomUrl.push(url)
                }
            }
    })
    return g5accessoryBenqcomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on G5 BenQ.com - accessory",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g5accessoryBenqcomUrlReturnedData = await g5accessoryBenqcomUrlCollect();
    console.log(g5accessoryBenqcomUrlReturnedData)
    for(let i =0; i<g5accessoryBenqcomUrlReturnedData.length; i++){
        const g5accessoryBenqcomUrlCheck = g5accessoryBenqcomUrlReturnedData[i]
        await this.page.goto("https://www.benq.com/en-us/index.html")
        await this.page.goto(g5accessoryBenqcomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        //if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0)
        if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0 || innerHtml.indexOf("g6-local-navigation-container")>0 || innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0 ){
            if(innerHtml.indexOf("buy-now-btn")>0){
                const buyButton = await this.page.$eval('#simpleFlow > span.buy-now-btn', element => element.innerHTML);
                console.log(g5accessoryBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
                pass.push(g5accessoryBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5accessoryBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
                pass.push(g5accessoryBenqcomUrlCheck)
            }else if(innerHtml.indexOf("g6-local-navigation-container")>0){
                const buyButton = await this.page.$eval('li.button > a', element => element.innerHTML);
                console.log(g5accessoryBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
                pass.push(g5accessoryBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5accessoryBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
                pass.push(g5accessoryBenqcomUrlCheck)
            }else{
                console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
                pass.push(g5accessoryBenqcomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g5accessoryBenqcomUrlCheck}`)
            fail.push(g5accessoryBenqcomUrlCheck)
        }
    }
    console.log("Benq.com pass url",pass)
    console.log("Benq.com fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //G5
// //accessory
// //Benq.com
// const g5accessoryBenqcomUrl=[]
// const g5accessoryBenqcomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "/accessory/"
//             if(url.indexOf(publishUrl)!==-1){
//                 if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
//                     g5accessoryBenqcomUrl.push(url)
//                 }
//             }
//     })
//     return g5accessoryBenqcomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on G5 BenQ.com - accessory",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g5accessoryBenqcomUrlReturnedData = await g5accessoryBenqcomUrlCollect();
//     console.log(g5accessoryBenqcomUrlReturnedData)
//     for(let i =0; i<g5accessoryBenqcomUrlReturnedData.length; i++){
//         const g5accessoryBenqcomUrlCheck = g5accessoryBenqcomUrlReturnedData[i]
//         await this.page.goto("https://www.benq.com/en-us/index.html")
//         await this.page.goto(g5accessoryBenqcomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);

//         if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g5accessoryBenqcomUrlCheck}`)
//             pass.push(g5accessoryBenqcomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g5accessoryBenqcomUrlCheck}`)
//             fail.push(g5accessoryBenqcomUrlCheck)
//         }
//     }
//     console.log("Benq.com pass url",pass)
//     console.log("Benq.com fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })

//projector-lamp
//Benq.com
const g5projectorLampBenqcomUrl=[]
const g5projectorLampBenqcomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "/projector/lamps/"
            if(url.indexOf(publishUrl)!==-1){
                if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
                    g5projectorLampBenqcomUrl.push(url)
                }
            }
    })
    return g5projectorLampBenqcomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on G5 BenQ.com - Projector Lamp",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g5projectorLampBenqcomUrlReturnedData = await g5projectorLampBenqcomUrlCollect();
    console.log(g5projectorLampBenqcomUrlReturnedData)
    for(let i =0; i<g5projectorLampBenqcomUrlReturnedData.length; i++){
        const g5projectorLampBenqcomUrlCheck = g5projectorLampBenqcomUrlReturnedData[i]
        await this.page.goto("https://www.benq.com/en-us/index.html")
        await this.page.goto(g5projectorLampBenqcomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0 || innerHtml.indexOf("g6-local-navigation-container")>0 || innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0){
            if(innerHtml.indexOf("buy-now-btn")>0){
                const buyButton = await this.page.$eval('#simpleFlow > span.buy-now-btn', element => element.innerHTML);
                console.log(g5projectorLampBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5SpeakerBenqcomUrlCheck}`)
                pass.push(g5projectorLampBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5projectorLampBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5projectorLampBenqcomUrlCheck}`)
                pass.push(g5projectorLampBenqcomUrlCheck)
            }else if(innerHtml.indexOf("g6-local-navigation-container")>0){
                const buyButton = await this.page.$eval('li.button > a', element => element.innerHTML);
                console.log(g5projectorLampBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5projectorLampBenqcomUrlCheck}`)
                pass.push(g5projectorLampBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5projectorLampBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5projectorLampBenqcomUrlCheck}`)
                pass.push(g5projectorLampBenqcomUrlCheck)
            }else{
                console.log(`Pass URL: ${g5projectorLampBenqcomUrlCheck}`)
                pass.push(g5projectorLampBenqcomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g5projectorLampBenqcomUrlCheck}`)
            fail.push(g5projectorLampBenqcomUrlCheck)
        }
    }
    console.log("Benq.com pass url",pass)
    console.log("Benq.com fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //projector-lamp
// //Benq.com
// const g5projectorLampBenqcomUrl=[]
// const g5projectorLampBenqcomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "/projector/lamps/"
//             if(url.indexOf(publishUrl)!==-1){
//                 if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
//                     g5projectorLampBenqcomUrl.push(url)
//                 }
//             }
//     })
//     return g5projectorLampBenqcomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on G5 BenQ.com - Projector Lamp",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g5projectorLampBenqcomUrlReturnedData = await g5projectorLampBenqcomUrlCollect();
//     console.log(g5projectorLampBenqcomUrlReturnedData)
//     for(let i =0; i<g5projectorLampBenqcomUrlReturnedData.length; i++){
//         const g5projectorLampBenqcomUrlCheck = g5projectorLampBenqcomUrlReturnedData[i]
//         await this.page.goto("https://www.benq.com/en-us/index.html")
//         await this.page.goto(g5projectorLampBenqcomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);

//         if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g5projectorLampBenqcomUrlCheck}`)
//             pass.push(g5projectorLampBenqcomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g5projectorLampBenqcomUrlCheck}`)
//             fail.push(g5projectorLampBenqcomUrlCheck)
//         }
//     }
//     console.log("Benq.com pass url",pass)
//     console.log("Benq.com fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })


//G5
//refurbished-computer-lights-lamps
//Benq.com
const g5RefurbishedBenqcomUrl=[]
const g5RefurbishedBenqcomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "https://www.benq.com/en-us/refurbished-computer-lights-lamps/"
            if(url.indexOf(publishUrl)!==-1){
                if(url.indexOf("refurbished-desk-lamps/")!==-1 || url.indexOf("refurbished-computer-monitor-lights/")!==-1){
                    if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
                        g5RefurbishedBenqcomUrl.push(url)
                    }
                }
            }
    })
    return g5RefurbishedBenqcomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on G5 BenQ.com - refurbished-computer-lights-lamps",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g5RefurbishedBenqcomUrlReturnedData = await g5RefurbishedBenqcomUrlCollect();
    console.log(g5RefurbishedBenqcomUrlReturnedData)
    for(let i =0; i<g5RefurbishedBenqcomUrlReturnedData.length; i++){
        const g5RefurbishedBenqcomUrlCheck = g5RefurbishedBenqcomUrlReturnedData[i]
        await this.page.goto("https://www.benq.com/en-us/index.html")
        await this.page.goto(g5RefurbishedBenqcomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0 || innerHtml.indexOf("g6-local-navigation-container")>0 || innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0){
            if(innerHtml.indexOf("buy-now-btn")>0){
                const buyButton = await this.page.$eval('#simpleFlow > span.buy-now-btn', element => element.innerHTML);
                console.log(g5RefurbishedBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
                pass.push(g5RefurbishedBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5RefurbishedBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
                pass.push(g5RefurbishedBenqcomUrlCheck)
            }else if(innerHtml.indexOf("g6-local-navigation-container")>0){
                const buyButton = await this.page.$eval('li.button > a', element => element.innerHTML);
                console.log(g5RefurbishedBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
                pass.push(g5RefurbishedBenqcomUrlCheck)
            }else if(innerHtml.indexOf('<div id="simpleFlow" class="button component-products-right-btn">Notify Me</div>')>0){
                const buyButton = await this.page.$eval('#simpleFlow', element => element.innerHTML);
                console.log(g5RefurbishedBenqcomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
                pass.push(g5RefurbishedBenqcomUrlCheck)
            }else{
                console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
                pass.push(g5RefurbishedBenqcomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g5RefurbishedBenqcomUrlCheck}`)
            fail.push(g5RefurbishedBenqcomUrlCheck)
        }
    }
    console.log("Benq.com pass url",pass)
    console.log("Benq.com fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //G5
// //refurbished-computer-lights-lamps
// //Benq.com
// const g5RefurbishedBenqcomUrl=[]
// const g5RefurbishedBenqcomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://www.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "https://www.benq.com/en-us/refurbished-computer-lights-lamps/"
//             if(url.indexOf(publishUrl)!==-1){
//                 if(url.indexOf("refurbished-desk-lamps/")!==-1 || url.indexOf("refurbished-computer-monitor-lights/")!==-1){
//                     if(url.indexOf("buy.html")<0 && url.indexOf("specifications.html")<0 && url.indexOf("question.html")<0 && url.indexOf("reviews.html")<0 && url.indexOf("spec.html")<0){
//                         g5RefurbishedBenqcomUrl.push(url)
//                     }
//                 }
//             }
//     })
//     return g5RefurbishedBenqcomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on G5 BenQ.com - refurbished-computer-lights-lamps",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g5RefurbishedBenqcomUrlReturnedData = await g5RefurbishedBenqcomUrlCollect();
//     console.log(g5RefurbishedBenqcomUrlReturnedData)
//     for(let i =0; i<g5RefurbishedBenqcomUrlReturnedData.length; i++){
//         const g5RefurbishedBenqcomUrlCheck = g5RefurbishedBenqcomUrlReturnedData[i]
//         await this.page.goto("https://www.benq.com/en-us/index.html")
//         await this.page.goto(g5RefurbishedBenqcomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);

//         if(innerHtml.indexOf("Add to Cart")>0 || innerHtml.indexOf("Out of Stock")>0 || innerHtml.indexOf("Discontinued")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g5RefurbishedBenqcomUrlCheck}`)
//             pass.push(g5RefurbishedBenqcomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g5RefurbishedBenqcomUrlCheck}`)
//             fail.push(g5RefurbishedBenqcomUrlCheck)
//         }
//     }
//     console.log("Benq.com pass url",pass)
//     console.log("Benq.com fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })

//Zowie
//camade
//mouse-skatez
//refurbished-gaming-esports-monitors
//monitor-accessory
//keyboard
//mouse-pad
//mouse
//monitor
const g5ZowiecomUrl=[]
const g5ZowiecomUrlCollect = async () => {
    try {
        // const specUrl=[]
        const result = await request.get("https://zowie.benq.com/en-us/sitemap.xml")
        const $ = cheerio.load(result)
        $("url > loc").each((index,element)=>{
            const url = $(element).text()
            const publishUrl = "https://zowie.benq.com/en-us/"
            if(url.indexOf(publishUrl)!==-1){
                if(url.indexOf("/camade/")!==-1 || url.indexOf("/mouse-skatez/")!==-1 || url.indexOf("/refurbished-gaming-esports-monitors/")!==-1 || url.indexOf("/monitor-accessory/")!==-1 || url.indexOf("/keyboard/")!==-1 || url.indexOf("/mouse-pad/")!==-1 || url.indexOf("https://zowie.benq.com/en-us/mouse/")!==-1 || url.indexOf("https://zowie.benq.com/en-us/monitor/")!==-1){
                    if(url.indexOf("product-configure.html")<0 && url.indexOf("warranty.html")<0 && url.indexOf("video.html")<0 && url.indexOf("faq.html")<0 && url.indexOf("warranty.html")<0 && url.indexOf("configuration.html")<0 && url.indexOf("product-edition-compare-configure.html")<0 && url.indexOf("download.html")<0 && url.indexOf("comparison-config-page.html")<0){
                        g5ZowiecomUrl.push(url)
                    }
                }
            }
    })
    return g5ZowiecomUrl;
    } catch (error) {
      console.log(error);
    }
};


Given("Check on Zowie",{timeout: 12000 * 5000},async function(){
    const pass=[]
    const fail=[]
    const g5ZowiecomUrlReturnedData = await g5ZowiecomUrlCollect();
    console.log(g5ZowiecomUrlReturnedData)
    for(let i =0; i<g5ZowiecomUrlReturnedData.length; i++){
        const g5ZowiecomUrlCheck = g5ZowiecomUrlReturnedData[i]
        await this.page.goto("https://zowie.benq.com/en-us/index.html")
        await this.page.goto(g5ZowiecomUrlCheck+cicGA)
        await this.page.waitForSelector('html')
        // const innerHtml = await page.$eval('body', element => element.innerHTML);
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // if(innerHtml.indexOf("Buy Now")>0 || innerHtml.indexOf("Notify Me")>0)
        if(innerHtml.indexOf('<button id="buy-btn" target="_self" class="buy-btn">Buy Now</button>')>0 || innerHtml.indexOf('<button data-toggle="modal" data-target="#notifyModal" class="notify" style="cursor: pointer;">Notify Me</button>')>0 || innerHtml.indexOf('buy-btn customize')>0){
            if(innerHtml.indexOf('<button id="buy-btn" target="_self" class="buy-btn">Buy Now</button>')>0){
                const buyButton = await this.page.$eval('#buy-btn', element => element.innerHTML);
                console.log(g5ZowiecomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5ZowiecomUrlCheck}`)
                pass.push(g5ZowiecomUrlCheck)
            }else if(innerHtml.indexOf('<button data-toggle="modal" data-target="#notifyModal" class="notify" style="cursor: pointer;">Notify Me</button>')>0){
                const buyButton = await this.page.$eval('button.notify', element => element.innerHTML);
                console.log(g5ZowiecomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5ZowiecomUrlCheck}`)
                pass.push(g5ZowiecomUrlCheck)
            }else if(innerHtml.indexOf('buy-btn customize')>0){
                const buyButton = await this.page.$eval('a.buy-btn', element => element.innerHTML);
                console.log(g5ZowiecomUrlCheck, buyButton)
                //console.log(`Pass URL: ${g5ZowiecomUrlCheck}`)
                pass.push(g5ZowiecomUrlCheck)
            }else{
                console.log(`Pass URL: ${g5ZowiecomUrlCheck}`)
                pass.push(g5ZowiecomUrlCheck)
            }
        }else{
            console.log(`Fail URL: ${g5ZowiecomUrlCheck}`)
            fail.push(g5ZowiecomUrlCheck)
        }
    }
    console.log("Zowie pass url",pass)
    console.log("Zowie fail url",fail)
    if(fail.length>0){
        throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
    }
})

// //Zowie
// //camade
// //mouse-skatez
// //refurbished-gaming-esports-monitors
// //monitor-accessory
// //keyboard
// //mouse-pad
// //mouse
// //monitor
// const g5ZowiecomUrl=[]
// const g5ZowiecomUrlCollect = async () => {
//     try {
//         // const specUrl=[]
//         const result = await request.get("https://zowie.benq.com/en-us/sitemap.xml")
//         const $ = cheerio.load(result)
//         $("url > loc").each((index,element)=>{
//             const url = $(element).text()
//             const publishUrl = "https://zowie.benq.com/en-us/"
//             if(url.indexOf(publishUrl)!==-1){
//                 if(url.indexOf("/camade/")!==-1 || url.indexOf("/mouse-skatez/")!==-1 || url.indexOf("/refurbished-gaming-esports-monitors/")!==-1 || url.indexOf("/monitor-accessory/")!==-1 || url.indexOf("/keyboard/")!==-1 || url.indexOf("/mouse-pad/")!==-1 || url.indexOf("https://zowie.benq.com/en-us/mouse/")!==-1 || url.indexOf("https://zowie.benq.com/en-us/monitor/")!==-1){
//                     if(url.indexOf("product-configure.html")<0 && url.indexOf("warranty.html")<0 && url.indexOf("video.html")<0 && url.indexOf("faq.html")<0 && url.indexOf("warranty.html")<0 && url.indexOf("configuration.html")<0 && url.indexOf("product-edition-compare-configure.html")<0 && url.indexOf("download.html")<0 && url.indexOf("comparison-config-page.html")<0){
//                         g5ZowiecomUrl.push(url)
//                     }
//                 }
//             }
//     })
//     return g5ZowiecomUrl;
//     } catch (error) {
//       console.log(error);
//     }
// };


// Given("Check on Zowie",{timeout: 12000 * 5000},async function(){
//     const pass=[]
//     const fail=[]
//     const g5ZowiecomUrlReturnedData = await g5ZowiecomUrlCollect();
//     console.log(g5ZowiecomUrlReturnedData)
//     for(let i =0; i<g5ZowiecomUrlReturnedData.length; i++){
//         const g5ZowiecomUrlCheck = g5ZowiecomUrlReturnedData[i]
//         await this.page.goto("https://zowie.benq.com/en-us/index.html")
//         await this.page.goto(g5ZowiecomUrlCheck+cicGA)
//         await this.page.waitForSelector('html')
//         // const innerHtml = await page.$eval('body', element => element.innerHTML);
//         const innerHtml = await this.page.$eval('html', element => element.innerHTML);

//         if(innerHtml.indexOf("Buy Now")>0 || innerHtml.indexOf("Notify Me")>0){
//             console.log(`Pass URL: ${g5ZowiecomUrlCheck}`)
//             pass.push(g5ZowiecomUrlCheck)
//         }else{
//             console.log(`Fail URL: ${g5ZowiecomUrlCheck}`)
//             fail.push(g5ZowiecomUrlCheck)
//         }
//     }
//     console.log("Zowie pass url",pass)
//     console.log("Zowie fail url",fail)
//     if(fail.length>0){
//         throw new Error(`以下這些URL的Buy Button消失: ${fail} `)
//     }
// })